

<HTML>
<HEAD>
    <TITLE>18ZALG - Ctvrtek 15:30</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 7.3.17
 * Time: 12:26
 */?>

<?php
require "../../../common/body_begin.php"
?>
<h1>Cviceni ZALG Ctvrtek 15:30 T-204</h1>
<h2>Prehled dochazky</h2>
<iframe id="dochazka" src="https://docs.google.com/spreadsheets/d/1nUzzq7jCcp-xSmNn_C692X7dKMS5yyN-GiQrRlfIb-M/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

<h2>1. Cviceni</h2>
<!--<a href="prez/zprocv1.pdf" target="_blank">Prezentace</a>-->
<a href="src/softwarovy_projekt.pdf" target="_blank">Softwarovy projekt ze skript ZALGu</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Organizacni zalezitosti</li>
    <li>Zivotni cyklus softwaroveho projektu</li>
</ul>


<h2>2. Cviceni</h2>
<!--<a href="prez/ZPROcv2_ct.pdf" target="_blank">Prezentace</a>-->
<!--<a href="src/zpro2_ct1_src.zip" target="_blank">Zdrojove soubory</a>-->
<p>
    Probrali jsme:

<ul>
    <li>Binarni vyhledavaci strom</li>
    <li>Implementace pomoci trid</li>
    <li>Trida Vrchol</li>
    <li>Trida Strom</li>
    <ul>
        <li>Mazani</li>
        <li>Pridavani</li>
    </ul>
</ul>

<h2>3. Cviceni</h2>
<!--<a href="prez/ZPROcv2_ct.pdf" target="_blank">Prezentace</a>-->
<a href="src/bvs_virius.zip" target="_blank">Implementace BVS (Virius)</a>
<p>
    Probrali jsme:

<ul>
    <li>Dokonceni BVS - mazani prvku ze stromu</li>
    <li>Rekurentni vztahy</li>
    <ul>
        <li>Ukazka na Fibonacciho posloupnosti</li>
    </ul>
</ul>


<h2>4. Cviceni</h2>
<p>
    Odpadlo

<h2>5. Cviceni</h2>
<!--<a href="prez/ZPROcv2_ct.pdf" target="_blank">Prezentace</a>-->
<a href="src/lififo.zip" target="_blank">Implementace LIFO a FIFO</a>
<p>
    Probrali jsme:

<ul>
    <li>LIFO - zasobnik</li>
    <li>FIFO - fronta</li>

</ul>

<h2>6. Cviceni</h2>
<h3 class="red"><a href="src/zapocty2012.pdf" target="_blank">Zadani zapoctovych praci (pujceno od Martina Hejtmanka)</a></h3>
<a href="src/binsearch.zip" target="_blank">Binarni vyhledavani</a>
<a href="src/hanoi.zip" target="_blank">Hanojske veze</a>
<a href="src/euklid.zip" target="_blank">Eukliduv algoritmus</a>
<a href="src/horner.zip" target="_blank">Hornerovo schema</a>
<p>
    Probrali jsme:

<ul>
    <li>Zadani zapoctovych uloh</li>
    <li>Binarni vyhledavani</li>
    <li>Hanojske veze</li>
    <li>Eukliduv algoritmus</li>
    <li>Hornerovo schema</li>
</ul>

<h2>7. Cviceni</h2>
<a href="src/frontakino.zip" target="_blank">Fronta do kina</a>
<a href="src/koza.zip" target="_blank">Vlk, Koza, Zeli</a>
<a href="src/damy.zip" target="_blank">8 dam</a>
<p>
    Probrali jsme:

<ul>
    <li>Implementace nekolika znamych problemu pomcoi backtrackingu</li>
</ul>

<h2>8. Cviceni</h2>
<a href="src/dijkstra.py" target="_blank">Dijkstruv algoritmus (Python3)</a>
<p>
    Probrali jsme:

<ul>
    <li>Dijkstruv algoritmus</li>
</ul>

<h2>9. Cviceni</h2>
<a href="src/postfix.py" target="_blank">Shunting yard a postfix (Python3)</a>
<p>
    Probrali jsme:

<ul>
    <li>Zpracovani aritmetickeho vyrazu</li>
    <li>Binarni strom</li>
    <li>Postfix, infix, prefix</li>
    <li>Shunting yard (zeleznicarsky) algoritmus a vyhodnoceni postfixu</li>
</ul>
<?php
require "../../../common/body_end.php"
?>
</HTML>
