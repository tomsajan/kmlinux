#!/usr/bin/env python3
import re

# priorities and operators
OPERATORS = {
    '+': 1,
    '-': 1,
    '*': 2,
    '/': 2,
    '^': 3,
}

# associativity
ASL = ('+', '-', '*', '/')
ASR = ('^',)


def tokenize(expr):
    """
    Returns list of tokens
    :param expr: raw expression 
    :return: 
    """

    def to_number(n):
        try:
            return float(n)
        except ValueError:
            return n

    r = re.compile(r"(\b\w*[\.]?\w+\b|[\(\)\+\*\-\/\^])")
    tokens = r.findall(expr)
    return list(map(to_number, tokens))


def shunting(tokens):
    """
    Shunting yard algorithm
    :param tokens: tokenized list
    :return: postfix token list
    """

    postfix = list()
    operator_stack = list()
    for token in tokens:
        if isinstance(token, (int, float)): # if it is a number
            postfix.append(token)
        elif token in OPERATORS:
            while operator_stack:    # not empty stack
                if token in OPERATORS and operator_stack[-1] in OPERATORS and (
                    token in ASL and OPERATORS[token] <= OPERATORS[operator_stack[-1]] or
                    token in ASR and OPERATORS[token] < OPERATORS[operator_stack[-1]]):
                        postfix.append(operator_stack.pop())  # pop from stack and push to output
                else:
                    break
            operator_stack.append(token)
        elif token == '(':
            operator_stack.append(token)
        elif token == ')':
            while operator_stack[-1] != '(':
                postfix.append(operator_stack.pop())
            operator_stack.pop()  # remove last '('
        else:
            raise ValueError("Unknown expression {}".format(token))

    # pop the rest of tokens in stack and push to output
    while operator_stack:
        postfix.append(operator_stack.pop())

    return postfix


def eval_postfix(postfix):
    """
    evaluates postfix expression
    :param postfix: list of tokens in postfix 
    :return: result value
    """

    def evaluate(op1, operator, op2):
        if operator == '+':
            return op1+op2
        elif operator == '-':
            return op1-op2
        elif operator == '*':
            return op1*op2
        elif operator == '/':
            return op1/op2
        elif operator == '^':
            return op1**op2

    val_stack = list()
    for token in postfix:
        if isinstance(token, (int, float)):
            val_stack.append(token)
        elif token in OPERATORS:
            # binary operators
            op2 = val_stack.pop()
            op1 = val_stack.pop()
            val_stack.append(evaluate(op1, token, op2))  # evaluate and return result to stack
        else:
            raise ValueError("Unknown expression {}".format(token))

    if len(val_stack) == 1:
        return val_stack[0]
    else:
        raise Exception("Undefined result")


if __name__ == '__main__':
    while True:
        user_input = input("Arithmetic expression: ")
        tokens = tokenize(user_input)
        print(tokens)
        postfix = shunting(tokens)
        print(postfix)
        result = eval_postfix(postfix)
        print(result)