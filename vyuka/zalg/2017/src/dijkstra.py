#! /usr/bin/env python3
from collections import defaultdict

DIRECTED_GRAPH = False


class Graph:
    """
    class representing a graph. Contains nodes, edges and distances
    """
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(set)
        self.distances = dict()

    def add_node(self, node):
        self.nodes.add(node)

    def add_edge(self, from_node, to_node, dist):
        self.distances[(from_node, to_node)] = dist
        self.edges[from_node].add(to_node)
        if not DIRECTED_GRAPH:
            self.distances[(to_node, from_node)] = dist
            self.edges[to_node].add(from_node)

    def get_dist(self, node_from, node_to):
        return self.distances[(node_from, node_to)]

    def get_neighbours(self, node):
        return self.edges[node]


def dijkstra(graph, initial):
    """
    implementation of dijkstra algorithm
    :param graph: graph instance 
    :param initial: initial node
    :return: path
    """
    nodes = set(graph.nodes)
    distances = {node: float('inf') for node in nodes}  # dict comprehension - creates dict - pairs of node and distance
    distances[initial] = 0
    path = {initial: None}

    while nodes:
        min_node = next(iter(nodes))  # extract first element from set
        for node in nodes:
            if distances[node] < distances[min_node]:
                min_node = node
        nodes.discard(min_node)  # remove min node from nodes

        min_dist = distances[min_node]
        for nei in graph.get_neighbours(min_node):
            if nei in nodes:        # not yet removed from set of nodes
                new_dist = min_dist + graph.get_dist(min_node, nei)
                if new_dist < distances[nei]:
                    distances[nei] = new_dist
                    path[nei] = min_node

    return path, distances


def print_path(path, distances, node_to):
    current_node = node_to
    output = ""
    while current_node is not None:
        output += current_node
        if path[current_node]:
            output += ">--"
        current_node = path[current_node]
    print(output[::-1])
    print(distances[node_to])

if __name__ == '__main__':
    g = Graph()
    # add nodes to graph
    for node in ('A', 'B', 'C', 'D', 'E', 'F'):
        g.add_node(node)

    # add edges to graph [from, to, dist]
    edges = (('A', 'B', 2),
             ('A', 'C', 4),
             ('B', 'C', 1),
             ('B', 'E', 5),
             ('B', 'D', 4),
             ('C', 'E', 3),
             ('D', 'F', 2),
             ('E', 'D', 3),
             ('E', 'F', 2),
             )
    for edge in edges:
        g.add_edge(*edge)   # * passes individual items of the edge to the function

    path, distances = dijkstra(g, 'A')
    print_path(path, distances, 'F')