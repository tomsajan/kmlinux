<HTML>
<HEAD>
    <TITLE>Jan Tomsa - vyuka predmetu 18ZALG</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../common/include.php";
    ?>
</HEAD>

<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 7.3.17
 * Time: 12:37
 */?>

<?php
require "../../common/body_begin.php"
?>

<h1>Vyuka a cviceni 18ZALG</h1>
<br>

<!--<h1 class="red">Zadani zapoctu <a href="2016/zapocet.php">ZDE</a></h1>-->
<!--<h1 class="red"><a href="2016/terminy.php">Terminy</a> odevzdavani</h1>-->

<h2>Skolni rok 2016/2017</h2>
<ul>
    <li><a href="2017/ct.php">Kruh ctvrtek 15:30 (T-204)</a></li>
</ul>

<h2 class="red"><a href="2017/src/zapocty2012.pdf" target="_blank">Zadani zapoctovych praci</a> </h2>


<h2>Podminky zapoctu</h2>
<div class="justify">
    <ul>
        <li>Vypracovat zapoctovou ulohu</li>
        <ul>
            <li>Zadani v behem semestru</li>
            <li>Kazdy bude mit jinou ulohu</li>
            <li>Doporucuji vypracovat do zacatku cervna (pak tu nebudu)</li>
            <li>(Temer) Libovolny programovaci jazyk</li>
        </ul>
        <li>Povinna dochazka</li>
        <li>3 absence</li>
        <li>Dalsi absence na dohode</li>
        <ul>
            <li>Nahradni uloha</li>
        </ul>
        <li>Obcasne povinne domaci ukoly</li>
    </ul>
</div>

<?php
require "../../common/body_end.php"
?>
</HTML>
