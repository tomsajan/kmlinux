<HTML>
<HEAD>
    <TITLE>Jan Tomsa - vyuka predmetu 18ZPRO</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../common/include.php";
    ?>
</HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 14.10.16
 * Time: 11:02
 */?>

<?php
require "../../common/body_begin.php"
?>

<h1>Vyuka a cviceni 18ZPRO</h1>
<br>

<!--<h1 class="red">Zadani zapoctu <a href="2016/zapocet.php">ZDE</a></h1>-->
<!--<h3 class="red"><a href="2016/terminy.php">Terminy</a> odevzdavani</h3>-->
<!--<h2 class="red">Zapocet je mozne ziskat az do konce letniho zkouskoveho. Individualni domluva.</h2>-->


<h2>Skolni rok 2016/2017</h2>
<ul>
    <li><a href="2016/po_1.php">Kruh pondeli 9:30 (T-115)</a></li>
    <li><a href="2016/ct_1.php">Kruh ctvrtek 11:30 (T-105)</a></li>
    <li><a href="2016/ct_2.php">Kruh ctvrtek 13:30 (B-009)</a></li>
</ul>

<h2>Skolni rok 2017/2018</h2>
<ul>
    <li><a href="2017/ut_1.php">Kruh utery 7:30 (T-105)</a></li>
    <li><h3 class="red"><a href="2017/zapocty.php">ZADANI ZAPOCTOVYCH PRACI</a> </h3></li>
</ul>

<h2>Podminky zapoctu</h2>
<div class="justify">
<ul>
    <li>Vypracovat 3 semestralni projekty/prace/programy</li>
    <li>Povinna dochazka</li>
    <li>2 absence</li>
    <li>Dalsi absence po predchozi omluve z vazneho duvodu</li>
    <li>Obcasne (ne)povinne domaci ukoly</li>
    <li>Moznost <a href="2016/express.php">expresniho</a> zapoctu</li>
</ul>
</div>


<h2>Doporucene vyvojove prostredi</h2>
<div class="justify">
    Pro bezproblemove a prijemne programovani v C/C++ doporucuji stahnout a nainstalovat vyvojove prostredi
    <a href="http://www.codeblocks.org/downloads/26" target="_blank">Code::Blocks</a>.
    Bez problemu funguje i na windows, jeho pouzivani je bez licencnich poplatku.
    Do windows si stahnete verzi 16.01 s mingw-nosetup. Staci jen rozbalit a zacit pouzivat. Neni nutne mit administratorsky
    pristup k pc. V baliku je i integrovany GCC prekladac. Pro jeho privetivost a kvuli sjednoceni vyuky cviceni a prednasek
    <span class="red">na toto prostredi prejdeme na vsech cvicenich.</span>
    Pokud nechcete hledat, ktery balicek presne stahnout, <a href="codeblocks-16.01mingw-nosetup.zip" target="_blank">ZDE</a> je primy odkaz na vyse zminovanou verzi.
    Pokud pouzivate linux, tak pro derivaty debianu (ubuntu, mate, apod) muzete pouzit: "sudo apt-get install codeblocks"
</div>

<?php
require "../../common/body_end.php"
?>
</HTML>
