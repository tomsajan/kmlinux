<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 11.11.17
 * Time: 11:06
 */
?>

<HTML>
<HEAD>
    <TITLE>Jan Tomsa - vyuka predmetu 18ZPRO - zadani zapoctu</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
require "../../../common/body_begin.php"
?>
<h1>Zapoctove prace 18ZPRO</h1>
<h2>Prvni zapoctova</h2>
<ul>
    <li>Seznam neceho - kytky, houby, zvirata, lidi.....</li>
        -> Pouzit struct a pole, tedy pole struktur
    <li>Rozumna staticka delka - cca 10, nebo dle libosti</li>
    <li>Interaktivita</li>
    <ul>
        <li>hlavni menu - switch</li>
        <li>moznost vypsat vsechny zaznamy</li>
        <li>pridani novych zaznamu</li>
        <li>jeste neco - co vymyslite, cim vic tim lip :)</li>
    </ul>
    <li>Na zacatku prazdny seznam</li>
    <li>Pozor na to, abyste neprekrocili max delku pole</li>
    <li>main.cpp - volani funkci z menu</li>
    <li>seznam.cpp - implementace funkci a seznamu</li>
    <li>seznam.h - hlavicky funkci a struktura</li>
    <li>odevzdat emailem - jen *.cpp a *.h soubory</li>
    <li class="red">do nedele 19.11.2017</li>
</ul>


<h2>Druha zapoctova</h2>
<ul>
    Naprogramovat hada<br>
    <li>Mozno pouzit zdrojove kody ze cviceni: <a href="src/7-hra.zip" target="_blank">Zdrojove soubory - hra</a></li>
    <li>Povinne:</li>
    <ul>
        <li>Implementace hada pomoci spojoveho seznamu</li>
        <ul>
            <li>Principy vcetne zdrojovych kodu seznamu jsme probirali na cvicenich a kody jsou dostupne ke stazeni - <a href="src/9.zip" target="_blank">Seznam</a> </li>
        </ul>
        <li>Vyuziti prace se soubory (zapis, cteni) - dle libosti (napr. ulozeni skore, ulozeni pozice hada, nebo treba bludiste apod, co vas napadne)</li>
        <li>rozdeleni na hlavickove .h a zdrojove .cpp soubory</li>
    </ul>
    <li>Bonus (dobrovolny):</li>
    <ul>
        <li>Implementace kontinualniho pohybu</li>
        <ul>
            <li>Princip, jakym dosahnout kontinualniho pohybu je nasledujici:</li>
            <ul>
                <li>hlavni sekce programu bude jeden nekonecny cyklus (treba while)</li>
                <li>v kazdem cyklu se zjisti aktualni cas</li>
                <li>zjistime jestli ubehl potrebny casovy interval pro akci (tj treba 200ms, zalezi na vas; tj pohyb hada o jednu bunku)
                    - pokud ano: podivame se jestli byla stisknuta nejaka klavesa, jestli jo, tak podle klavesy zmenime smer pohybu, jinak smer pohybu nechame tak jak byl predtim. Posuneme hadem danym smerem</li>
            </ul>
            <li>Ukazka pouziti casovani a neblokujici detekce klaves -  <a href="src/8.zip" target="_blank">Time</a> </li>
        </ul>
    </ul>
    <li>Cim vice vlastnich napadu do programu zanesete, tim lip :)</li>
    <li>Termin odevzdani - behem zkouskoveho.</li>
    <ul>
        <li>Domluvime se na terminu osobni schuzky, kde program probereme.</li>
    </ul>
    <li>Nevahejte me kontaktovat s cimkoliv si nebudete vedet rady :)</li>
</ul>


<?php
require "../../../common/body_end.php"
?>
</HTML>
