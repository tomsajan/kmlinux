<HTML>
<HEAD>
    <TITLE>18ZPRO - Utery 7:30</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 9.10.17
 * Time: 11:02
 */?>

<?php
require "../../../common/body_begin.php"
?>
<h1>Cviceni ZPRO Utery 7:30 T-105</h1>
<h2>Prehled dochazky</h2>
<iframe id="dochazka" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSLS8CDpNNXwmYtHdj_7VHvZGwcnyy1IjEXfW5kAOjniUAd7yWASVmyYUXiGMotupUVZBaBueqGa7oX/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

<h3 class="red"><a href="zapocty.php">ZADANI ZAPOCTOVYCH PRACI</a> </h3>

<h2>1. Cviceni</h2>
<a href="prez/zprocv1.pdf" target="_blank">Prezentace</a>
<a href="src/zprocv1.zip" target="_blank">Zdrojove soubory</a>

<p>
    <h3>Probrali jsme:</h3>

<ul>
    <li>Seznameni s C/C++</li>
    <li>zakladni datove typy (int, float, bool)</li>
<!--    <li>string</li>-->
<!--    <li>namespace (std)</li>-->
<!--    <li>podminky - if, else</li>-->
<!--    <li>while cyklus; break; continue</li>-->
    <li>vstupy a vystupy do konzole (cin, cout)</li>
    <li>struktura projektu</li>
</ul>

Nepovinny DU - objem a obsah kvadru


<h2>2. Cviceni</h2>
<a href="prez/ZPROcv2.pdf" target="_blank">Prezentace</a>
<a href="src/2.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>While</li>
    <li>Do-While</li>
    <li>Rizeni cyklu (break, continue)</li>
    <!--    <li>namespace (std)</li>-->
    <!--    <li>podminky - if, else</li>-->
    <!--    <li>while cyklus; break; continue</li>-->
    <li>Funkce</li>
    <li>Rozdeleni projektu do vice souboru</li>
</ul>



<h2>3. Cviceni</h2>
<a href="prez/ZPROcv3.pdf" target="_blank">Prezentace</a>
<a href="src/3.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Opakovani z minula</li>
    <li>Hlavni menu programu</li>
    <li>Procvicovani</li>
</ul>

Nepovinny DU - doplnit statistiku o suda a licha cisla a mocniny 2 a 7

<h2>4. Cviceni</h2>
<a href="prez/zpro4.pdf" target="_blank">Prezentace</a>
<a href="src/4.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Scope promenne</li>
    <li>Debugovani</li>
    <li>Switch</li>
    <li>For cyklus</li>
    <li>Pole</li>
</ul>


<h2>5. Cviceni</h2>
<a href="prez/zpro5.pdf" target="_blank">Prezentace</a>
<a href="src/5.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Opakovani pole</li>
    <li>2D pole</li>
    <li>Struktury</li>
    <li>Terminalova hra</li>
</ul>

<h2>6. Cviceni</h2>
<a href="prez/zpro6.pdf" target="_blank">Prezentace</a>
<a href="src/6.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Ternarni operator</li>
    <li>Pointery</li>
</ul>


<h2>7. Cviceni</h2>
<a href="prez/zpro7.pdf" target="_blank">Prezentace</a>
<a href="src/7.zip" target="_blank">Zdrojove soubory</a>
<a href="src/7-hra.zip" target="_blank">Zdrojove soubory - hra</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Zapis a cteni ze souboru</li>
    <li>Hra</li>
</ul>

<h2>8. Cviceni</h2>
<a href="prez/zpro8.pdf" target="_blank">Prezentace</a>
<a href="src/8.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Pointery a pole</li>
    <li>Dynamicke promenne - new, delete</li>
    <li>Spojovy seznam</li>
    <li>zjistovani ubehleho casu</li>
    <li>neblokujici detekce stisku klavesy</li>
</ul>

<h2>9. Cviceni</h2>
<a href="prez/zpro9.pdf" target="_blank">Prezentace</a>
<a href="src/9.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Dalsi funkce seznamu</li>
    <li>Pretezovani funkci</li>
    <li>Vychozi zapametry funkci</li>
    <li>Parametry funkce main</li>
</ul>


<h2>10. Cviceni</h2>
<a href="prez/zpro10.pdf" target="_blank">Prezentace</a>
<a href="src/10.zip" target="_blank">Zdrojove soubory</a>

<p>
<h3>Probrali jsme:</h3>

<ul>
    <li>Rekurze</li>
    <li>Conway's game of life</li>
</ul>




<?php
require "../../../common/body_end.php"
?>
</HTML>
