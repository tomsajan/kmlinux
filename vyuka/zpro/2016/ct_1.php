<HTML>
<HEAD>
    <TITLE>18ZPRO - Ctvrtek 11:30</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 14.10.16
 * Time: 11:02
 */?>

<?php
require "../../../common/body_begin.php"
?>
<h1>Cviceni ZPRO Ctvrtek 11:30 T-105</h1>
<h2>Prehled dochazky</h2>
<iframe id="dochazka" src="https://docs.google.com/spreadsheets/d/1lvHJpcemQFz4aSaq7bFMfpTUXl9kmKd6cjevIG_axvw/pubhtml"></iframe>

<h2>1. Cviceni</h2>
<a href="prez/zprocv1.pdf" target="_blank">Prezentace</a>
<a href="src/zpro1_src.zip" target="_blank">Zdrojove soubory</a>

<p>
    <h3>Probrali jsme:</h3>

<ul>
    <li>Seznameni s C/C++</li>
    <li>zakladni datove typy (int, float)</li>
    <li>string</li>
    <li>namespace (std)</li>
    <li>podminky - if, else</li>
    <li>while cyklus; break; continue</li>
    <li>vstupy a vystupy do konzole (cin, cout)</li>
</ul>

<h3>Domaci ukol:</h3><br>
Odevzdejte do pristi hodiny (20.10) emailem. Pokud byste meli nejake problemy, nevahejte me vcas kontaktovat, nejak to vyresime. ;-)
<ul>
    <li>Interaktivni program</li>
    <li>Pozdravi a zepta se uzivatele na jmeno (ulozi do promenne - string)</li>
    <li>Zepta se na 3 delky stran trojuhelnika - cele cislo - int</li>
    <li>Overi trojuhelnikovou nerovnost - a+b>c, b+c>a, c+a>b</li>
    <li>Pokud to je trojuhelnik, vypise delku jeho obvodu (a+b+c)</li>
    <li>Nakonec se program rozlouci s uzivatelem jeho jmenem a ukonci se</li>
</ul>
<h3>Mohli byste potrebovat:</h3>
<ul>
    <li>#include &lt;string&gt;</li>
    <li>using namespace std;</li>
    <li>cin &gt;&gt;</li>
    <li>cout &lt;&lt; </li>
    <li>if () {} else {}</li>
</ul>

<h2>2. Cviceni</h2>
<a href="prez/ZPROcv2_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro2_ct1_src.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>For cyklus, do-while cyklus</li>
    <li>Funkce - max, min, kv. rovnice, faktorial, vypis</li>
    <li>Debugging</li>
</ul>

<h2>3. Cviceni</h2>
<a href="prez/zprocv3.pdf" target="_blank">Prezentace</a>
<a href="src/ct3_pol.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Rozdeleni programu do zdrojovych (.cpp) a hlavickovych (.h) souboru</li>
    <li>Pole (array)</li>
    <ul>
        <li>inicializace</li>
        <li>pouziti</li>
    </ul>
    <li>Vicedimenzionalni pole</li>
    <li>Pole jako parametr funkce</li>
</ul>


<h2>4. Cviceni</h2>
<a href="prez/zpro4_ct.pdf" target="_blank">Prezentace</a>
<a href="src/ct4_1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Switch, case</li>
    <li>Struktury</li>
    <li>Enum</li>
    <li>Zacatek Eratosthenova sito</li>
</ul>

<h3>Domaci ukol:</h3><br>
Odevzdejte do pristi hodiny (10.11) emailem. Pokud byste meli nejake problemy, nevahejte me vcas kontaktovat, nejak to vyresime. ;-)
<ul>
    <li>Vytvorte knihovnu (cpp + h soubory), ktera implementuje databazi telefonniho seznamu</li>
    <li>Pouzijte uvodni rozhrani k programu ze cviceni. Upravde volby dle potreb noveho programu</li>
    <li>Definujte novou strukturu, ktera bude reprezentovat zaznam v telefonnim seznamu (jmeno, prijmeni, cislo, nebo i dalsi polozky, ktere se vam budou zamlouvat)</li>
    <li>Vytvorte pole nasich novych struktur s rozumnou kapacitou (10), globalni promenna v ramci knihovny. Extern nemusite pouzivat, staci to definovat v ramci knihovny.</li>
    <li>Program bude umet nasledujici:</li>
    <ul>
        <li>Uzivatel bude moci zvolit "Pridat novy zaznam" a to zavola funkci, ktera se uzivatele zepta na detaily noveho kontaktu a kontakt ulozi</li>
        <li>Moznost vypsat cely seznam (Dejte pozor na maximalni delku pole a na aktualne zaplnenou kapacitu (nechcete vypisovat prazdna pole)</li>
        <li>Moznost ukoncit program</li>
        <li>Dobrolovne cokoli dalsiho vas napadne</li>
    </ul>
    <li>Mnoho klicovych soucasti mame v kodech ze cviceni</li>
</ul>


<h2>5. Cviceni</h2>
<a href="prez/zpro5_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro5_ct1.zip" target="_blank">Zdrojove soubory</a>
Zdrojove soubory budou dostupne pozdeji
<p>
    Probrali jsme:

<ul>
    <li>Rozsah platnosti promenne (scope)</li>
    <ul>
        <li>
            Promenne lokalni
        </li>
        <li>Promenne globalni</li>
        <li>Promenne formalni</li>
    </ul>
    <li>Modifikatory promennych</li>
    <ul>
        <li>short, long, unsigned, signed</li>
        <li>static, const, extern</li>
    </ul>
    <li>Ternarni operator ? :</li>
    <li>Program pro vypsani prvocisel (Eratoshenovo sito)</li>
</ul>

<h2>6. Cviceni</h2>
<p>Odpadlo</p>

<h2>7. Cviceni</h2>
<a href="prez/zpro7_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro7_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Prace se soubory</li>
    <ul>
        <li>Zapis do souboru</li>
        <li>Cteni ze souboru</li>
    </ul>
    <li>Programy</li>
    <ul>
        <li>Derivace</li>
        <li>Matice a nasobeni matic</li>
        <li>Vypocet PI Leibnitzovou formuli</li>
    </ul>
</ul>



<h3>Domaci ukol:</h3><br>
Odevzdejte do nedele (4.12) emailem. Pokud byste meli nejake problemy, nevahejte me vcas kontaktovat. ;-)
<ul>
    <li>Vyuzijte kody z minuleho ukolu (telefonni seznam) a implementujte dalsi 2 volby</li>
    <ul>
        <li>Ulozit seznam do souboru</li>
        <li>Nacist seznam ze souboru</li>
    </ul>
    <li>Zakladni verze bude umet alespon ulozit cely seznam do predem urceneho souboru</li>
    <li>Pokrocilejsi verze bude umet z toho souboru seznam nasledne nacist (prepise aktualni seznam daty ze souboru)</li>
    <li>Jeste pokrocilejsi verze se uzivatele bude ptat na nazev souboru, do ktereho nebo ze ktereho nacitat.</li>
    <li>Pro referenci muzete vyuzit soubory ze cviceni, u uzivatelskeho zadavani nazvu souboru se muze hodit funkce .c_str() u stringu jako vstup do funkce open (viz funkce v programu s maticemi)</li>
    <li>Pridal jsem updatovany program matice, ktery lepe ilustruje ukladani matic do souboru. V nem je pouzita zminena funkce .c_str()</li>
</ul>

<h2>8. Cviceni</h2>
<a href="prez/zpro8_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro8_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Pointery</li>
    <ul>
        <li>Operator & - adresa</li>
        <li>Operator * - dereference</li>
        <li>Deklarace pointeru</li>
        <li>Nulove pointery</li>
        <li>Pointer jako parametr funkce (swap, max_mean)</li>
        <li>Modifikator const pro pointer</li>
        <li>Vztah pointeru a pole</li>
        <li>Operator hranate zavorky</li>
        <li>Pocitani s pointery</li>
        <li>Dynamicke promenne a operatory new, new[] a delete, delete[]</li>

    </ul>
    <li>Program na dynamicke pole</li>
</ul>



<h2>9. Cviceni</h2>
<a href="src/zpro9_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Pointery - opakova</li>
    <li>2D pole (matice) - dynamicka pole</li>
    <li>Dalsi vyuziti pointeru - datove struktury</li>
</ul>


<h3>Domaci ukol:</h3><br>
Odevzdejte zdrojovy kod do nedele (18.12.) emailem. Pokud budete posilat celou slozku, smazte z ni .exe soubor, jinak mi to neprijde. Nejlepe tedy poslat jen zdrojove soubory.
<ul>
    <li>Vychozi kody k ukolu: <a href="src/matice_du_ct1.zip" target="_blank">Zdrojove soubory</a></li>
    <li>Pouzijte kody z odkazu vyse.</li>
    <li>Doplne funkci pro soucet dvou matic</li>
    <ul>
        <li>Vstupni parametry funkce budou 2 ukazatele na matici (Struktury TMatice)</li>
        <li>Vystup z funkce bude ukazatel na novou matici, ktera vznikne jako soucet matic na vstupu</li>
        <li>Kontrolujte, ze rozmery obou matic na vstupu maji stejne rozmery, jinak neni soucet mozny</li>
        <li>Soucet matic je definovat jako soucet po prvcich (<a href="https://cs.wikipedia.org/wiki/S%C4%8D%C3%ADt%C3%A1n%C3%AD_matic#Sou.C4.8Det_po_prvc.C3.ADch" target="_blank">Wiki</a>) </li>
    </ul>
    <li>Doplne funkci pro vypis matice do souboru</li>
    <ul>
        <li>Vstupni parametr bude ukazatel na matici, kterou vytisknout a string, ktery bude obsahovat cestu, do jakeho to ulozit souboru</li>
        <li>Format souboru bude presne dany</li>
        <ul>
            <li>Prvni radek bude obsahovat 2 cisla - pocet radku a pocet sloupku</li>
            <li>Jeden volny radek</li>
            <li>Matice vytiskla po radcich</li>
            <li>Ukazkovy vystupni soubor: <a href="src/matice.txt" target="_blank">ZDE</a> </li>
        </ul>
    </ul>
    Dodrzujte strukturu projektu, inspirujte se v jiz hotovych funkcich.
</ul>



<h2>10. Cviceni</h2>
<a href="prez/zpro10.pdf" target="_blank">Prezentace</a>
<a href="src/zpro10_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Rekurze</li>
    <ul>
        <li>Faktorial</li>
        <li>Fibonacci</li>
    </ul>
    <li>Linearni spojovy seznam a la Vanocni retez</li>
    <li>Zacatek vanocniho binarniho vyhledavaciho stromu</li>
</ul>

<h2>11. Cviceni</h2>
<a href="prez/zpro11_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro11_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Dokonceni BVS</li>
    <li>Reference</li>
    <ul>
        <li>Reference jako vstupni parametr</li>
        <li>Reference jako vystupni parametr</li>
    </ul>
    <li>Pretezovani funkci</li>
    <li>Vychozi parametry funkci</li>
    <li>Parametry funkce main</li>
</ul>

<h2>12. Cviceni</h2>
<a href="prez/zpro12_ct.pdf" target="_blank">Prezentace</a>
<a href="src/zpro12_ct1.zip" target="_blank">Zdrojove soubory</a>
<p>
    Probrali jsme:

<ul>
    <li>Uvod do OOP (objektove orientovane programovani)</li>
    <li>Tridy a objekty</li>
    <li>Funkce v ramci objektu</li>
    <li>Konstruktory</li>
    <li>Destruktor</li>
    <li>Dedeni</li>
</ul>

<?php
require "../../../common/body_end.php"
?>
</HTML>
