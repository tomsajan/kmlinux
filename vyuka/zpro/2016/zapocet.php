<HTML>
<HEAD>
    <TITLE>18ZPRO - zadani zapocet</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
require "../../../common/body_begin.php"
?>
<h3>Terminy odevzdavani <a href="terminy.php">ZDE</a> </h3>

<h1>Zapocet 18ZPRO</h1>

<ul>
    <li>Naprogramovat knihovnu implementujici jedno/obou smerny linearni spojovy seznam (obdoba vanocniho retezu)</li>
    <ul>
        <li>To implikuje pouziti hlavickoveho .h a zdrojoveho .cpp souboru obstaravajici funkce seznamu, a soubor .cpp s funkci main, kde bude jen pouziti seznamu.</li>
    </ul>
    <li>Vyber toho, co bude ulozeno v seznamu, necham na vas. Muze to byt cislo, muze to by telefonni seznam, databaze knizek, ... cokoliv vam bude prijemne.</li>
    <li>Volbu varianty (jedno nebo obousmerny) necham na vas</li>
    <li>Do seznamu bude mozne ulozit libovolne mnozstvi dat</li>
    <li>Budu si davat pozor na uvolnovani pameti - korektni mazani prvku pomoci delete tam, kde se vytvareji pomoci new. Na konci programu tedy bude potreba seznam korektne smazat.</li>
    <li>Ze seznamu funkci musite implementovat alespon 5</li>
    <li>Na ostatni funkce se mohu zeptat u odevzdavani programu</li>
    <li>Dobrovolnosti se meze nekladou - muzete implementovat i jine funkce, ktere se vam budou hodit do projektu</li>
    <li>Kratky priklad pouziti implementovane knihovny</li>
    <li>Seznam moznych funkci:</li>
    <ul>
        <li>Vypsani seznamu</li>
        <li>Vlozeni prvku na zacatek</li>
        <li>Vlozeni prvku na konec</li>
        <li>Najiti prvku s danou hodnotou</li>
        <li>Smazani prvku</li>
        <li>Otoceni seznamu (prvky se prepoji tak, ze budou pospojovana v opacnem poradi)</li>
        <li>Najiti maxima, minima - nebo jine relevantni hodnoty vzhledem k vasemu projektu</li>
        <li>Ulozeni seznamu do souboru</li>
        <li>Nacteni seznamu ze souboru</li>
        <li>Serazeni seznamu (treba podle velikost urcite hodnoty v prvku)</li>
    </ul>
</ul>


<br>
<h3 class="green">Pokud neco nebude jasne, nebo s necim budete potrebovat pomoct, nevahejte mi napsat a domluvit si konzultaci.</h3>
<h2 class="red">Odevzdavani ulohy po predchozi domluve terminu emailem nebo na hromadnych terminech, ktere vypisu behem zkouskoveho.</h2>



<?php
require "../../../common/body_end.php"
?>
</HTML>
