<HTML>
<HEAD>
    <TITLE>Jan Tomsa - vyuka predmetu 18ZPRO - expresni zapocet</TITLE>
    <?php
    error_reporting( E_ALL );
    require "../../../common/include.php";
    ?>
</HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 14.10.16
 * Time: 11:02
 */?>

<?php
require "../../../common/body_begin.php"
?>
<h1>Expresni zapocet 18ZPRO</h1>

<ul>
    <li>Naprogramovat knihovnu implementujici jedno/obou smerny linearni spojovy seznam</li>
    <li>Volbu varianty necham na vas</li>
    <li>Alespon 5 metod pro praci s daty (prvky)</li>
    <li>Napriklad:</li>
    <ul>
        <li>Vypsani seznamu</li>
        <li>Vlozeni prvku na zacatek</li>
        <li>Vlozeni prvku na konec</li>
        <li>Vlozeni prvku za dany prvek s danymi daty</li>
        <li>Smazani prvku</li>
        <li>Najiti hodnoty</li>
        <li>Ulozeni do souboru</li>
        <li>Nacteni ze souboru</li>
        <li>Vytvoreni seznamu s opacne razenymi prvky</li>
        <li>Najiti maxima, minima</li>
        <li>dalsi dle libosti ... </li>
    </ul>
    <li>Kratky priklad pouziti implementovane knihovny</li>
    <li>Kod mi pak poslete a domluvime se na terminu konzultace, kde mi program predvedete a ja se budu ptat doplnujici otazky.</li>
</ul>

Kdo bude mit zajem o expresni zapocet, necht se ozve emailem a domluvime podrobnosti. Idelani by bylo stihnout expresni zapocet nekdy behem  listopadu.



<?php
require "../../../common/body_end.php"
?>
</HTML>
