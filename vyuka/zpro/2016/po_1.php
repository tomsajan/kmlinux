<HTML>
    <HEAD>
        <TITLE>18ZPRO - Pondeli 9:30</TITLE>
        <?php
        error_reporting( E_ALL );
        require "../../../common/include.php";
        ?>
    </HEAD>


<?php
/**
 * Created by PhpStorm.
 * User: janik
 * Date: 14.10.16
 * Time: 11:02
 */?>

    <?php
        require "../../../common/body_begin.php"
    ?>
    <h1>Cviceni ZPRO Pondeli 9:30 T-115</h1>
    <h2>Prehled dochazky</h2>
    <iframe id="dochazka" src="https://docs.google.com/spreadsheets/d/1wa3g24oM3KWgVZl1gJUzldeucNCnCzePou-bp5REs6U/pubhtml"></iframe>

    <h2>1. Cviceni</h2>
    <a href="prez/zprocv1.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro1_src.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

        <ul>
            <li>Seznameni s C/C++</li>
            <li>zakladni datove typy (int, float)</li>
            <li>string</li>
            <li>namespace (std)</li>
            <li>podminky - if, else</li>
            <li>while cyklus; break; continue</li>
            <li>vstupy a vystupy do konzole (cin, cout)</li>
        </ul>


    <h2>2. Cviceni</h2>
    <a href="prez/ZPROcv2_po.pdf" target="_blank">Prezentace</a>
    <a href="src/zprocv2_src.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>For cyklus, do-while cyklus</li>
        <li>Funkce - max, min, kv. rovnice, faktorial</li>
        <li>Debugging</li>
        <li>Scope (rozsah platnosti promenne)</li>
    </ul>


    <h3>Domaci ukol:</h3><br>
    Odevzdejte zdrojovy kod do pristi hodiny (24.10) emailem. Pokud byste meli nejake problemy, nevahejte me vcas kontaktovat, nejak to vyresime. ;-)
    <ul>
        <li>Vyuzijte funkce faktorial ze cviceni pro implementaci funkce "kombinacni cislo"</li>
        <li>Kombinacni cislo, "n nad k" je definovano nasledovne:</li>
    </ul>
    <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/eec648e9c75cdff024051f6ace3a2ebd8ed6d1a5">
    <ul>
        <li>Funkce bude mit 2 vstupni parametry - n a k - cela cisla (int)</li>
        <li>Vystup bude jedno cele cislo (int) dle definice vyse</li>
        <li>Jde vlastne jen o pouziti ifu na rozdeleni vypoctu dle rozdvojeni v definici a nasledne pouziti hotove funkce faktorial</li>
        <li>Vice informaci o kombinacnim cislu naleznete na <a href="https://cs.wikipedia.org/wiki/Kombina%C4%8Dn%C3%AD_%C4%8D%C3%ADslo">wikipedii</a>, stejne tak o <a href="https://cs.wikipedia.org/wiki/Faktori%C3%A1l">faktorialu</a> </li>
        <li>Dobrovolne rozsireni:</li>
        <ul>
            <li>Pro overeni spravnosti se muzete pokusit naprogramovanou funkci pouzit pro overeni prvnich trech vlastnosti kombinacnich cisel z wikipedie</li>
            <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/532ea86fd5c1d5ebb1a5dea20719d0234790683b">
            <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/39458c88586c1c78131eb39549579411fb9f5b0e">
            <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/1b82832fa78480bb157758aee0fa9fb6f3a71568">
        </ul>
    </ul>

    <h2>3. Cviceni</h2>
    <a href="prez/zprocv3.pdf" target="_blank">Prezentace</a>
    <a href="src/po3.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Rozdeleni programu do zdrojovych (.cpp) a hlavickovych (.h) souboru</li>
        <li>Pole (array)</li>
        <ul>
            <li>inicializace</li>
            <li>pouziti</li>
        </ul>
        <li>Vicedimenzionalni pole</li>
        <li>Pole jako parametr funkce</li>
        <li>Textovy retezec jako char[]</li>
        <li>Ukazka prevodu velkeho znaku na maly a opacne</li>

    </ul>
    <h3>Domaci ukol:</h3><br>
    Odevzdejte zdrojovy kod do pristi hodiny (31.10) emailem. Pokud byste s ukolem meli nejake trable, nevahejte napsat ;-)
    <ul>
        <li>Naprogramujte funkci "toUpper", ktera prevede text zadany z klavesnice (staci jedno slovo) na VELKA pismena</li>
        <li>Nacitani vstupu z klavesnice bude ve funkci main</li>
        <li>Funkce na prevod textu bude mit minimalne jeden parametr - char[], tedy pole znaku, ve kterem bude ulozene slovo nactene z klavesnice</li>
        <li>Funkce prevedeny text jen vytiskne na obrazovku, nemusi mit zadnou navratovou hodnotu (void)</li>
        <li>Funkce si musi poradit s ruznou velikosti pismen: napr. vstup "aHoJ" - vystup "AHOJ"</li>
        <li>Pro referenci pouzijte <a href="https://cs.wikipedia.org/wiki/ASCII">ASCII</a> tabulku </li>
        <li>pripadne patricne kapitoly na <a href="https://www.tutorialspoint.com/cplusplus/">Tutorials point</a> o polich a stringach</li>
        <li>Inspirujte se ve zdrojovych kodech ze cviceni</li>
    </ul>

    <h2>4. Cviceni</h2>
    <p>Odpadlo z duvodu zmeny rozvrhu</p>

    <h2>5. Cviceni</h2>
    <a href="prez/zpro5_po.pdf" target="_blank">Prezentace</a>
    <a href="src/cv5_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Switch, case</li>
        <li>Struktury</li>
        <li>Enum</li>
        <li>Zacatek knihovny pro Komplexni cisla</li>
    </ul>

    <h2>6. Cviceni</h2>
    <p>Odpadlo</p>

    <h2>7. Cviceni</h2>
    <a href="prez/zpro7_po.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro7_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Rozsah platnosti promenne</li>
        <li>Modifikatory promennych</li>
        <ul>
            <li>signed, unsigned, long, short</li>
            <li>static, const, extern</li>
        </ul>
        <li>Ternarni operator ? :</li>
        <li>Prace se soubory</li>
        <ul>
            <li>Zapis do souboru</li>
            <li>Cteni ze souboru</li>
        </ul>
        <li>Eratosthenovo sito</li>
    </ul>


    <h2>8. Cviceni</h2>
    <a href="prez/zpro8_po.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro8_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Program na numerickou derivaci</li>
        <li>Pointery</li>
        <ul>
            <li>Operator & - adresa</li>
            <li>Operator * - dereference</li>
            <li>Deklarace pointeru</li>
            <li>Nulove pointery</li>
            <li>Pointer jako parametr funkce (swap, max_mean)</li>
            <li>Modifikator const pro pointer</li>
            <li>Operator sipka (->) pro pristup k pameti slozenych objektu</li>
        </ul>
    </ul>


    <h2>9. Cviceni</h2>
    <a href="src/zpro9_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Pointery - dokonceni</li>
        <ul>
            <li>Vztah pole a pointeru</li>
            <li>Dynamicka pamet</li>
            <ul>
                <li>Operatory new, new[] a delete, delete[]</li>
                <li>Ukazka dynamicke pole</li>
                <li>Ukazka dynamicke 2D pole</li>
            </ul>
        </ul>
    </ul>

    <h3>Domaci ukol:</h3><br>
    Odevzdejte zdrojovy kod do pristi hodiny (12.12.) emailem. Pokud budete posilat celou slozku, smazte z ni .exe soubor, jinak mi to neprijde. Nejlepe tedy poslat jen zdrojove soubory.
    <ul>
        <li>Vychozi kody k ukolu: <a href="src/matice_du.zip" target="_blank">Zdrojove soubory</a></li>
        <li>Pouzijte kody z odkazu vyse.</li>
        <li>Doplne funkce pro soucet dvou matic</li>
        <ul>
            <li>Vstupni parametry funkce budou 2 ukazatele na matici (Struktury TMatice)</li>
            <li>Vystup z funkce bude ukazatel na novou matici, ktera vznikne jako soucet matic na vstupu</li>
            <li>Kontrolujte, ze rozmery obou matic na vstupu maji stejne rozmery, jinak neni soucet mozny</li>
            <li>Soucet matic je definovat jako soucet po prvcich (<a href="https://cs.wikipedia.org/wiki/S%C4%8D%C3%ADt%C3%A1n%C3%AD_matic#Sou.C4.8Det_po_prvc.C3.ADch" target="_blank">Wiki</a>) </li>
        </ul>
        <li>Doplne funkci pro vypis matice do souboru</li>
        <ul>
            <li>Vstupni parametr bude ukazatel na matici, kterou vytisknout a string, ktery bude obsahovat cestu, co jakeho to ulozit souboru</li>
            <li>Format souboru bude presne dany</li>
            <ul>
                <li>Prvni radek bude obsahovat 2 cisla - pocet radku a pocet sloupku</li>
                <li>Jeden volny radek</li>
                <li>Matice vytiskla po radcich</li>
                <li>Ukazkovy vystupni soubor: <a href="src/matice.txt" target="_blank">ZDE</a> </li>
            </ul>
        </ul>
        Dodrzujte strukturu projektu, inspirujte se v jiz hotovych funkcich.
    </ul>


    <h2>10. Cviceni</h2>
    <a href="prez/zpro10.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro10_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Rekurze</li>
        <ul>
            <li>Faktorial</li>
            <li>Fibonacci</li>
        </ul>
        <li>Linearni spojovy seznam a la Vanocni retez</li>
        <li>Zacatek vanocniho binarniho vyhledavaciho stromu</li>
    </ul>


    <h2>11. Cviceni</h2>
    <a href="prez/zpro11_po.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro11_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Dokonceni BVS</li>
        <li>Reference</li>
        <ul>
            <li>Reference jako vstupni parametr</li>
            <li>Reference jako vystupni parametr</li>
        </ul>
        <li>Pretezovani funkci</li>
        <li>Ukazka pretizeni operatoru</li>
        <li>Vychozi parametry funkci</li>
        <li>Parametry funkce main</li>
    </ul>


    <h2>12. Cviceni</h2>
    <a href="prez/zpro12_po.pdf" target="_blank">Prezentace</a>
    <a href="src/zpro12_po.zip" target="_blank">Zdrojove soubory</a>
    <p>
        Probrali jsme:

    <ul>
        <li>Uvod do OOP (objektove orientovane programovani)</li>
        <li>Tridy a objekty</li>
        <li>Funkce v ramci objektu</li>
        <li>Konstruktory</li>
        <li>Destruktor</li>
        <li>Dedeni</li>
    </ul>


    <?php
        require "../../../common/body_end.php"
    ?>
</HTML>
